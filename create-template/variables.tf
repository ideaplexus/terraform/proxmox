variable "proxmox-host" {
  type        = string
  description = "Proxmox host were the template should be created"
}

variable "proxmox-ssh-user" {
  type        = string
  description = "Proxmox SSH user with sufficient permissions"
  default     = "root"
}

variable "proxmox-ssh-password" {
  type        = string
  description = "Proxmox SSH password for the user. If not set, agent will be used"
  default     = ""
}



variable "proxmox-storage" {
  type        = string
  description = "Proxmox storage were the template should be stored"
  default     = "local-lvm"
}

variable "proxmox-template-vmid" {
  type        = number
  description = "Proxmox virtual machine id for the template"
}

variable "proxmox-template-name" {
  type        = string
  description = "Proxmox virtual machine name"
}

variable "proxmox-template-url" {
  type         = string
  description = "Proxmox virtual machine name"
}
